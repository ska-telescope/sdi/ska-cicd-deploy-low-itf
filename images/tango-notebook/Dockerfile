FROM jupyter/scipy-notebook:ubuntu-20.04

RUN mamba install --quiet --yes \
    ipywidgets \
    itango \
    jupyter_bokeh \
    jupyterlab \
    opencv \
    pytango \
    && mamba clean --all -f -y && \
    fix-permissions "${CONDA_DIR}" && \
    fix-permissions "/home/${NB_USER}"

RUN /opt/conda/bin/pip install --extra-index-url https://artefact.skao.int/repository/pypi-all/simple ska-control-model

RUN git clone -b main https://gitlab.com/tango-controls/jupyTango.git /opt/conda/var/jupyTango

RUN mkdir /opt/conda/var/ipython
ENV IPYTHONDIR=/opt/conda/var/ipython

RUN ipython profile create jupytango
RUN echo "c.InteractiveShellApp.extensions = ['jupytango']" >> $IPYTHONDIR/profile_jupytango/ipython_config.py
RUN python -m ipykernel install --sys-prefix --name jupytango --display-name "jupyTango" --profile jupytango

RUN cp /opt/conda/var/jupyTango/resources/logo/* /opt/conda/share/jupyter/kernels/jupytango

ENV PYTHONPATH=/opt/conda/var/jupyTango
ENV JUPYTER_CONTEXT=LAB
